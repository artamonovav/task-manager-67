package ru.t1.artamonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.artamonov.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.artamonov.tm.model.Project;
import ru.t1.artamonov.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.artamonov.tm.api.endpoint.IProjectRestEndpoint")
public class ProjectEndpointImpl implements IProjectRestEndpoint {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project
    ) {
        projectService.remove(project);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        projectService.removeById(id);
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<Project> findAll() {
        return projectService.findAll();
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public @Nullable Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return projectService.findOneById(id);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save")
    public Project save(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project
    ) {
        projectService.add(project);
        return project;
    }

}
