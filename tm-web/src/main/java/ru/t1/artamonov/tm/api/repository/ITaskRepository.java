package ru.t1.artamonov.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.artamonov.tm.model.Task;

@Repository
public interface ITaskRepository extends JpaRepository<Task, String> {
}
