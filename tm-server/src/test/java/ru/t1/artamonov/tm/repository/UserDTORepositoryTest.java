//package ru.t1.artamonov.tm.repository;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.*;
//import org.junit.experimental.categories.Category;
//import ru.t1.artamonov.tm.api.repository.dto.IUserDTORepository;
//import ru.t1.artamonov.tm.api.service.IConnectionService;
//import ru.t1.artamonov.tm.api.service.IPropertyService;
//import ru.t1.artamonov.tm.dto.model.UserDTO;
//import ru.t1.artamonov.tm.marker.UnitCategory;
//import ru.t1.artamonov.tm.repository.dto.UserDTORepository;
//import ru.t1.artamonov.tm.service.ConnectionService;
//import ru.t1.artamonov.tm.service.PropertyService;
//
//import javax.persistence.EntityManager;
//
//import static ru.t1.artamonov.tm.constant.UserTestData.USER1;
//import static ru.t1.artamonov.tm.constant.UserTestData.USER2;
//
//@Category(UnitCategory.class)
//public final class UserDTORepositoryTest {
//
//    @Nullable
//    private static EntityManager entityManager;
//
//    @Nullable
//    private static IUserDTORepository userRepository;
//
//    @BeforeClass
//    public static void init() {
//        @NotNull IPropertyService propertyService = new PropertyService();
//        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
//        entityManager = connectionService.getEntityManager();
//        userRepository = new UserDTORepository(entityManager);
//    }
//
//    @AfterClass
//    public static void connectionClose() {
//        entityManager.close();
//    }
//
//    @Before
//    public void transactionStart() {
//        entityManager.getTransaction().begin();
//    }
//
//    @After
//    public void transactionEnd() {
//        entityManager.getTransaction().rollback();
//    }
//
//    @Test
//    public void add() {
//        Assert.assertNull(userRepository.findOneById(USER1.getId()));
//        userRepository.add(USER1);
//        Assert.assertNotNull(userRepository.findOneById(USER1.getId()));
//    }
//
//    @Test
//    public void findAll() {
//        add();
//        Assert.assertTrue(userRepository.findAll().size() > 0);
//    }
//
//    @Test
//    public void findOneById() {
//        Assert.assertNull(userRepository.findOneById(USER1.getId()));
//        add();
//        @Nullable UserDTO user = userRepository.findOneById(USER1.getId());
//        Assert.assertNotNull(user);
//        Assert.assertEquals(USER1.getId(), user.getId());
//        Assert.assertNull(userRepository.findOneById(USER2.getId()));
//    }
//
//    @Test
//    public void findByLogin() {
//        Assert.assertNull(userRepository.findByLogin(USER1.getLogin()));
//        add();
//        Assert.assertEquals(USER1.getId(), userRepository.findByLogin(USER1.getLogin()).getId());
//        Assert.assertNull(userRepository.findByLogin(USER2.getLogin()));
//    }
//
//    @Test
//    public void findByEmail() {
//        Assert.assertNull(userRepository.findByEmail(USER1.getEmail()));
//        add();
//        Assert.assertEquals(USER1.getId(), userRepository.findByEmail(USER1.getEmail()).getId());
//        Assert.assertNull(userRepository.findByEmail(USER2.getEmail()));
//    }
//
//    @Test
//    public void remove() {
//        add();
//        Assert.assertNotNull(userRepository.findOneById(USER1.getId()));
//        userRepository.remove(USER1);
//        Assert.assertNull(userRepository.findOneById(USER1.getId()));
//    }
//
//    @Test
//    public void update() {
//        add();
//        @NotNull UserDTO user = userRepository.findOneById(USER1.getId());
//        Assert.assertNotNull(user);
//        user.setLogin("UNIT_TEST");
//        user.setFirstName("FIRST_NAME");
//        user.setLastName("LAST_NAME");
//        user.setMiddleName("MIDDLE_NAME");
//        user.setEmail("E@MAIL.RU");
//        userRepository.update(user);
//        @NotNull UserDTO updatedUser = userRepository.findOneById(USER1.getId());
//        Assert.assertNotNull(updatedUser);
//        Assert.assertEquals(user.getLogin(), updatedUser.getLogin());
//        Assert.assertEquals(user.getEmail(), updatedUser.getEmail());
//        Assert.assertEquals(user.getLastName(), updatedUser.getLastName());
//        Assert.assertEquals(user.getFirstName(), updatedUser.getFirstName());
//        Assert.assertEquals(user.getMiddleName(), updatedUser.getMiddleName());
//    }
//
//}
