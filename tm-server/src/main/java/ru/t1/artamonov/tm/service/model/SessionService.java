package ru.t1.artamonov.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.artamonov.tm.api.repository.model.ISessionRepository;
import ru.t1.artamonov.tm.api.repository.model.IUserRepository;
import ru.t1.artamonov.tm.api.service.model.ISessionService;
import ru.t1.artamonov.tm.exception.entity.ModelNotFoundException;
import ru.t1.artamonov.tm.exception.entity.UserNotFoundException;
import ru.t1.artamonov.tm.exception.field.IdEmptyException;
import ru.t1.artamonov.tm.exception.field.UserIdEmptyException;
import ru.t1.artamonov.tm.exception.user.AccessDeniedException;
import ru.t1.artamonov.tm.model.Session;
import javax.transaction.Transactional;
import java.util.List;

@Service
@NoArgsConstructor
public class SessionService implements ISessionService {

    @NotNull
    @Autowired
    private ISessionRepository sessionRepository;

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Session add(@Nullable Session model) {
        if (model == null) throw new AccessDeniedException();
        sessionRepository.saveAndFlush(model);
        return model;
    }

    @Override
    @SneakyThrows
    @Transactional
    public @NotNull Session add(@Nullable String userId, @Nullable Session model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new AccessDeniedException();
        model.setUser(userRepository.findById(userId).orElse(null));
        sessionRepository.saveAndFlush(model);
        return model;
    }

    @Override
    @SneakyThrows
    @Transactional
    public @NotNull List<Session> add(@NotNull List<Session> models) {
        if (models == null) throw new ModelNotFoundException();
        sessionRepository.saveAll(models);
        return models;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@NotNull String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        sessionRepository.deleteByUserId(userId);
    }

    @Override
    public boolean existsById(@NotNull String id) {
        return sessionRepository.existsById(id);
    }

    @Override
    public @Nullable List<Session> findAll() {
        return sessionRepository.findAll();
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return sessionRepository.findById(id).orElse(null);
    }

    @Override
    public @Nullable Session findOneById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return sessionRepository.findByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Session update(@Nullable Session model) {
        if (model == null) throw new AccessDeniedException();
        sessionRepository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Session remove(@Nullable Session model) {
        if (model == null) throw new AccessDeniedException();
        sessionRepository.delete(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Session removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session session = findOneById(id);
        if (session == null) throw new AccessDeniedException();
        sessionRepository.delete(session);
        return session;
    }

    @Override
    @SneakyThrows
    @Transactional
    public @NotNull Session removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session session = findOneById(userId, id);
        if (session == null) throw new AccessDeniedException();
        sessionRepository.delete(session);
        return session;
    }

}
