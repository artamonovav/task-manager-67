package ru.t1.artamonov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.artamonov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Transactional
    void deleteByUserId(String userId);

    boolean existsByUserIdAndId(String userId, String id);

    @Nullable
    List<Task> findAllByUserId(String userId);

    @Nullable
    List<Task> findAllByUserIdAndProjectId(String userId, String projectId);

    @Nullable
    Task findByUserIdAndId(String userId, String id);

    @Transactional
    void deleteByUserIdAndId(String userId, String id);

    long countByUserId(String userId);

    @Nullable
    Task findOneByUserIdAndId(String userId, String taskId);

    @Nullable
    @Query("SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY :sortType")
    List<Task> findAllByUserIdWithSort(@NotNull @Param("userId") String userId, @NotNull @Param("sortType") String sortType);

    @Transactional
    void deleteByProjectId(String projectId);

}
