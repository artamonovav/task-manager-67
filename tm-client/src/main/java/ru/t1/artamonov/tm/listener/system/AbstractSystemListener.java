package ru.t1.artamonov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.api.service.IPropertyService;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.listener.AbstractListener;

@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected AbstractListener[] listeners;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
